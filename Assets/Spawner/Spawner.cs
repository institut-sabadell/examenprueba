using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public GameObject[] gameObjects;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(spawnejar());


    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public IEnumerator spawnejar()
    {
        while (true)
        {
            int rand = Random.Range(0, 2);
            GameObject newGameObject = Instantiate(gameObjects[rand]);
            float randX = Random.Range(-8.5f, 5);
            newGameObject.transform.position = new Vector2(randX, 2);

            yield return new WaitForSeconds(1.5f);//trivial

        }
    }

}
