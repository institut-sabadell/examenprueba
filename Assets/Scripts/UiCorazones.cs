using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UIElements;

public class UiCorazones : MonoBehaviour
{
    [SerializeField]
    public Pelota jugador;
    public Pala palo;

    // Start is called before the first frame update
    void Start()
    {
        this.GetComponent<TextMeshProUGUI>().text = "Vida del jugador: " + jugador.vida;
        jugador.damage += MostrarVida;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void MostrarVida()
    {
        this.GetComponent<TextMeshProUGUI>().text = "Vida del jugador: " + jugador.vida;
        palo.transform.position = new Vector2(0, -5);

        if (jugador.vida <= 0)
        {
            Destroy(palo);
        }
        
    }
}
