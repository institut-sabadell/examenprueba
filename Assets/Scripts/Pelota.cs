using System.Collections;
using System.Collections.Generic;
using UnityEditor.SearchService;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Pelota : MonoBehaviour
{
    // Start is called before the first frame update
    private Rigidbody2D rb;
    public int velocidadMov;
    public float X;
    public float Y;
    public delegate void vidaDelegate();
    public event vidaDelegate damage;
    public int vida = 3;
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        rb.transform.Rotate(0, 0, Random.Range(1, 360));
        rb.velocity = this.transform.up * velocidadMov;
    }

    // Update is called once per frame
    void Update()
    {
        X = rb.velocity.x;
        Y = rb.velocity.y;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag=="drecha" || collision.gameObject.tag=="izquierda")
        {
            rb.velocity = new Vector2(-X, Y);
        }
        else
        {
            rb.velocity=new Vector2(X,-Y);
        }

        if(collision.gameObject.tag=="bloc")
        {
            Destroy(collision.gameObject);
        }

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag=="suelo")
        {
            morir();
            
        }

    }
    public void morir()
    {
        if (vida > 1)
        {
            this.transform.position = new Vector2(0, 0);
            rb = GetComponent<Rigidbody2D>();
            rb.transform.Rotate(0, 0, Random.Range(1, 360));
            rb.velocity = this.transform.up * velocidadMov;
            vida--;
            damage?.Invoke();
           
        }
        else
        {
            Destroy(gameObject);

            vida--;
            Debug.Log("Mi vida" + vida);
            damage?.Invoke();
            SceneManager.LoadScene("Arkanoid");
        }
      
    }

   
}
