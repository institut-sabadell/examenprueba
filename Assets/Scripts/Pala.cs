using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;

public class Pala : MonoBehaviour
{
    public Vector2 direccionMovimiento;
    public GameObject pelota;
    public int spd;
    private Rigidbody2D rb;
    
   
   
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        
    }

    // Update is called once per frame
    void Update()
    {
        direccionMovimiento = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical")).normalized; // Esto son valores de -1 a 1
       
    }
    private void FixedUpdate()
    {
        rb.MovePosition(rb.position + direccionMovimiento * spd * Time.fixedDeltaTime);

    }


   
    

}
